﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//Custom inspector that draws the default inspector and adds some elements below it
//Also applies some materials when values change
[CustomEditor(typeof(CurveParameters))]
public class CurveParametersEditor : Editor
{
    private CurveParameters script;

    void OnEnable()
    {
        script = (CurveParameters)target;
    }

    override public void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        DrawDefaultInspector();

        if (script.curves.Count == 0) EditorGUILayout.HelpBox("Add atleast one curve to the list", MessageType.Info);

        EditorGUILayout.Space();

        if (EditorGUI.EndChangeCheck())
        {
            script.LUT = CurveLUT.Create(script.curves);

            //Assign the texture to the debug mats to allow instant previewing, without having to save the texture to disk (slow)
            foreach (Material mat in script.materials)
            {
                mat.mainTexture = script.LUT;
                mat.SetFloat("_NumRows", script.curves.Count);
            }
        }

        using (new EditorGUILayout.HorizontalScope())
        {
            //Creates the LUT from the curves and saves it to disk
            if (GUILayout.Button("Save curve LUT"))
            {
                script.LUT = CurveLUT.Create(script.curves, "Assets/CurveLUT.png");

                //Assign the texture to the debug mats to allow instant previewing, without having to save the texture to disk (slow)
                foreach (Material mat in script.materials)
                {
                    mat.mainTexture = script.LUT;
                    mat.SetFloat("_NumRows", script.curves.Count);
                }
            }
            GUILayout.FlexibleSpace();

        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Output", EditorStyles.boldLabel);

        Rect cRect = EditorGUILayout.GetControlRect();
        using (new EditorGUILayout.HorizontalScope())
        {
            if (script.LUT) EditorGUI.DrawPreviewTexture(new Rect(cRect.x, cRect.y, 150f, 150f), script.LUT, null, ScaleMode.ScaleAndCrop);

        }
        GUILayout.Space(140f);
    }
}
