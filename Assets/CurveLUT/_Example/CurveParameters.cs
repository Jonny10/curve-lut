﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

//Example class to hold input curves
public class CurveParameters : MonoBehaviour
{
    public List<AnimationCurve> curves = new List<AnimationCurve>();

    public Texture2D LUT;

    public Material[] materials;

}
