﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
//Sets the row as an instanced property to the material
public class InstancedMaterialProperty : MonoBehaviour {

    [Range(1,5)]
    public int row = 1;

    private MeshRenderer r;
    private Material mat;
    private MaterialPropertyBlock props;

    private void OnEnable()
    {
        if (r == null) r = GetComponent<MeshRenderer>();
        if (mat == null) mat = r.sharedMaterial;
        if (props == null) props = new MaterialPropertyBlock();
    }

    // Update is called once per frame
    void Update () {
        if (!mat) return;

        props.SetFloat("_Row", row);

        r.SetPropertyBlock(props);
    }
}
