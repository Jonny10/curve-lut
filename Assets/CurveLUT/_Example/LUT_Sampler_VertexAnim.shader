﻿Shader "Unlit/LUT_Sampler"
{
    Properties
    {
        [NoScaleOffset]_MainTex ("Texture", 2D) = "white" {}
		[Space]
        _NumRows("Number of rows", Range(0,10)) = 0
        _Column("Column", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _NumRows;
            float _Column;

			//GPU instancing
			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(fixed, _Row)
			UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert (appdata v)
            {
                v2f o;
                
				float2 samplePoint;

				//Example uses Time to scrub through the gradient
				samplePoint.x = fmod(_Time.y, 1) + _Column;

				//Sample at center of gradient
                samplePoint.y = (_NumRows - 1.0)/_NumRows * _Row + 1.0/(2.0 * _NumRows);

				//Sample the LUT at the given row and column
                float s = tex2Dlod(_MainTex, float4(samplePoint.xy ,0,0));

				//Move all vertices up and scale
				v.vertex.y += s;
				v.vertex.xyz *= s;

                o.vertex = UnityObjectToClipPos(v.vertex);

				//Set the vertex colors to the gradient value
				o.color = s;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				return i.color;
            }
            ENDCG
        }
    }
}
