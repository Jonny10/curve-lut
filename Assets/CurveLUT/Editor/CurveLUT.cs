﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

//Editor class, pass a list of AnimationCurves and a LUT is returned as a Texture2D
public class CurveLUT : Editor {

    //Resolutions lower than 256 will start to show a grey line at the bottom
    //High resolutions aren't needed thanks to bilinear interpolation between texels
    private const int resolution = 256;

    public static Texture2D Create(List<AnimationCurve> curves, string path = null)
    {
        if (curves.Count == 0) return null;

        Texture2D LUT = new Texture2D(resolution, resolution, TextureFormat.ARGB32, false, true)
        {
            name = "LUT",
            anisoLevel = 0,
            filterMode = FilterMode.Bilinear,
            wrapMode = TextureWrapMode.Repeat

        };

        //Row height in pixels
        int rowHeight = resolution / curves.Count;

        int i = 0;
        int wPos = 0;
        int hPos = 0;

        float val = 0;

        for (int x = 0; x < resolution; x++)
        {
            wPos = x;

            for (int y = 0; y <= resolution; y++)
            {
                hPos = y;

                //0-1 value, distance from left to right
                float t = (float)wPos / (float)resolution;

                // Continue to next element when rowHeight has been exceeded
                i = (hPos / rowHeight);

                if (i < curves.Count)
                {
                    //Sample current curve at t (0-1)
                    val = curves[i].Evaluate(t);

                    //Can sample another set of curves here and store it in the other color channels

                    //Apply to current pixel
                    LUT.SetPixel(x, resolution - y, new Color(val, val, val, 1f));
                }
            }
        }

        LUT.Apply();

        if (path != null)
        {
            //Encode
            byte[] bytes = LUT.EncodeToPNG();

            //Create file
            File.WriteAllBytes(path, bytes);

            //Import file
            AssetDatabase.Refresh();
            LUT = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
        }

        return LUT;
    }
}
